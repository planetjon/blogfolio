			<?php do_action( 'blogfolio_after_content' ) ?>
			</div><!-- .container -->
		</main><!-- #page-content -->

		<footer id="site-footer">
			<div class="container">
				<?php do_action( 'blogfolio_site_footer' ) ?>
			</div>
		</footer>

		<?php wp_footer() ?>
		</div><!-- .page-container -->
	</body>
</html>
