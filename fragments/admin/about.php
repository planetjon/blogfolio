<h3>About Blogfolio</h3>

<p>Blogfolio is a lightweight HTML5 theme geared towards portfolio-based blogging, with responsive design for mobile to large displays.
Features include a custom homepage template with tiled blog posts, and several convenient widgetable areas.
Make it yours with a custom menu, header image, and background. Extend it through the numerous action hooks and custom template fragment system.
Visit the official <a target="_blank" title="Blogfolio project page" href="http://portfolio.planetjon.ca/projects/blogfolio/">Blogolio project page</a> for more information.
</p>

<p>A lot of hard work goes into maitaining this theme, providing your site visitors with the best possible browsing experience.
If you like Blogfolio, whaddaya say to buying me a beer?
</p>

<img src="<?php echo get_template_directory_uri() ?>/screenshot.png" alt="Screenshot of Blogfolio"/>

<div class="buy-me-a-beer">
<a target="_blank" rel="nofollow" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=X634254JN6KLG"><img src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" alt="" /></a>
</div>
