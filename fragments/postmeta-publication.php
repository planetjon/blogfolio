<div class="post-meta post-publication">
	<span>Published</span>
	<span class="entry-author data">by <?php the_author_posts_link() ?></span>
	<span class="entry-date data">on <?php echo get_the_date(); ?></span>
</div>
