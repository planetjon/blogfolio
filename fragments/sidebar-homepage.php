<?php if( is_active_sidebar( 'home-page-widget-container' ) ) : ?>
<div class="home-page-widget-container widget-container container">
	<?php dynamic_sidebar( 'home-page-widget-container' ) ?>
</div>
<?php endif ?>
