<div class="site-attribution">
	<span class="copyright"><?php printf( '&copy; %s %s', date( 'Y' ), get_bloginfo( 'name' ) ) ?></span>

	<?php if( Blogfolio::config( 'credit-in-footer' ) ) : ?>
	<span class="footer-credit"> - Powered by <a href="https://planetjon.ca/projects/blogfolio/" title="Blogfolio project page" target="_blank">Blogfolio</a></span>
	<?php endif ?>
</div>
