<?php if( is_active_sidebar( 'content-sidebar' ) ) : ?>
<aside class="content-sidebar widget-container container">
	<?php dynamic_sidebar( 'content-sidebar' ) ?>
</aside>
<?php endif ?>
